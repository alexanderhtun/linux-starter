######################################################################
# THIS BASH SHELL SCRIPT IS USED TO TEST THE SQLITE3 CRUD 
# PLEASE DO NOT USE IN PRODUCTION ENVRIONMNET  
# JUST FOR FUN 
# A.HTUN 
#####################################################################

echo "Enter the dbname"
read dbname
echo "Enter the tablename" 
read mytab

if [ ! -f $dbname.db ]; then 

cmd="CREATE TABLE $mytab(
id INTEGER PRIMARY KEY AUTOINCREMENT, 
software_id VARCHAR(10) NOT NULL, 
version_id VARCHAR(10) NOT NULL, 
release_id VARCHAR(10) NOT NULL,
status INT NOT NULL,
update_date TEXT NOT NULL
);"

sqlite3 $dbname.db << EOF 
$cmd 
EOF
fi 
export dbname 
export mytab 

bash show.sh $dbname,$mytab 

echo "1. Do you want to insert the new information into database" 
echo "2. Do you want to update the database"  
echo "3. Do you want to delete the row in database" 
read selectme
case $selectme in 
	1)
		bash query.sh $dbname,$mytab
		bash show.sh $dbname,$mytab
		;;
	2)
		bash update.sh $dbname,$mytab
		bash show.sh $dbname,$mytab
		;;
	3)
		bash delete.sh $dbname,$mytab
		bash show.sh $dbname,$mytab
		;;
	*)
		echo "Nothing to do, Thank you" 
		;;
esac 

