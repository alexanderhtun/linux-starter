##########################################################
# THIS SHELL USE THE EXPORT VALUE FROM THE CREATETABLE.SH
# NOT TO USE THIS BASH SCRIPT ONLY 
# PLEASE NOT TO TRY IN PRODUCTION ENVRIONMENT 
#########################################################


echo "Enter the id to update the status" 
read id
echo "ONLY CHANGE IN ONCE TIME" 
echo "1. update the software id" 
echo "2. update the version id" 
echo "3. update the release id" 
echo "4. update the status " 
echo "5. update the date" 

case $selectme in
	1)
	echo "Enter the software id" 
	read msoftware_id 
	update="UPDATE $mytab SET software_id=$msoftware_id WHERE id=$id LIMIT 1"
		;;
	2)
	echo "Enter the version id" 
	read mversion_id 
	update="UPDATE $mytab SET version_id=$mversion_id WHERE id=$id LIMIT 1"
		;;
	3)
	echo "Enter the release id" 
	read mrelease_id
	update="UPDATE $mytab SET release_id=$mrelease_id WHERE id=$id LIMIT 1"
		;;
	4)
	echo "Enter the new value to set the status" 
	read mstatus 
	update="UPDATE $mytab SET status=$mstatus WHERE id=$id LIMIT 1"
		;;
	5)
	echo "Enter the new date"
	read new_date 
	update="UPDATE $mytab SET status=$new_date WHERE id=$id LIMIT 1"
		;;
	*)
		echo "There is no update in the table" 
		;;
esac	

if [ -f $dbname.db ]; then 
	sqlite3 $dbname.db << EOF 
        $update
EOF
fi 
